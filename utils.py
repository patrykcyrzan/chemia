import pandas as pd
from sklearn.preprocessing import MinMaxScaler


def load_clean_data():
    try:
        dataset = pd.read_csv('input/clean_data.csv')
    except Exception as ex:
        print('Problem podczas odczytu pliku. Pamiętaj aby najpierw uruchomic plik, ktory przygotowuje dane!')
        raise
    # dataset.drop(['Cout', 'CO2', 'dP'], axis=1, inplace=True)
    dataset = dataset[['Cin', 'EC', 'theta']]
    # target = dataset['EC']
    # scaler = MinMaxScaler()
    # scaled = scaler.fit_transform(target)
    # dataset['EC'] = scaled
    return dataset


def get_4_classes_from_target(target):
    target_classes = target.copy()
    for index, row in target_classes.iterrows():
        re_output = row['EC']
        val = ''
        if re_output <= 0.20:
            val = 0
        elif 0.20 < re_output <= 0.50:
            val = 1
        elif 0.50 < re_output <= 0.80:
            val = 2
        elif re_output > 0.80:
            val = 3

        target_classes.at[index, 'EC'] = val
    return target_classes.astype(int).iloc[:, -1]


def get_2_classes_from_target(target):
    target_classes = target.copy()
    for index, row in target_classes.iterrows():
        re_output = row['EC']
        val = ''
        if re_output < 0.50:
            val = 0
        elif re_output > 0.50:
            val = 1

        target_classes.at[index, 'EC'] = val
    return target_classes.astype(int).iloc[:, -1]
